
//#include "../../vfs/inc/vfs.hpp"
#include "../../vfs/inc/FileSystemInterface.h"
#include "../../vfs/inc/FileSystem.h"
#include "../../vfs/inc/CommandParserInterface.h"
#include "../../vfs/inc/CommandParser.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace testing;


TEST(CommandParserTest, CommandParserTestTokenizeString)
{
    unique_ptr<CommandParserInterface> cp = unique_ptr<CommandParserInterface>(new CommandParser());
    vector<string> command;
    command = cp->readCommand("ls testdir");

    EXPECT_EQ(
    "ls",    
    command[0]
    );

}

struct FileSystemTest : public Test
{
  // FileSystem fs;
   unique_ptr<CommandParserInterface> cp;
    FileSystemTest() {
      
      cp = unique_ptr<CommandParserInterface>(new CommandParser());
    }

    ~FileSystemTest()
    {

    }

};

TEST_F(FileSystemTest, FileSystemTestMakeDirectory)
{

    vector<string> cmd;
  //  cp->mountFileSystem(fs);
    cmd = cp->readCommand("mkdir raj");

    EXPECT_TRUE(
    cp->executeCommand(cmd)
    );    
}

TEST_F(FileSystemTest, FileSystemTestChangeDirectory)
{

    vector<string> cmd;
  //  cp->mountFileSystem(fs);

    cmd = cp->readCommand("cd /");

    EXPECT_TRUE(
    cp->executeCommand(cmd)
    );

    
}

TEST_F(FileSystemTest, FileSystemTestChangeDirectoryToRoot)
{

    vector<string> cmd;
  //  cp->mountFileSystem(fs);

    cmd = cp->readCommand("cd /");

  
    EXPECT_TRUE(
     cp->executeCommand(cmd)
    );

    
}

TEST_F(FileSystemTest, FileSystemTestListDirectoryCommand)
{

    vector<string> cmd;
 //   cp->mountFileSystem(fs);
    cmd = cp->readCommand("ls");

    EXPECT_TRUE(  
    cp->executeCommand(cmd)
    );
    
                
}

TEST_F(FileSystemTest, FileSystemTestListRootDirectoryCommand)
{

    vector<string> cmd;
    //cp->mountFileSystem(fs);
    cmd = cp->readCommand("ls /");

    EXPECT_TRUE(  
    cp->executeCommand(cmd)
    );
    
                
}


TEST_F(FileSystemTest, FileSystemTestUnknownCommand)
{

    vector<string> cmd;
   // cp->mountFileSystem(fs);
    cmd = cp->readCommand("unknown testdir");

    EXPECT_EQ(
    false,  
    cp->executeCommand(cmd)
    );
}


