#ifndef COMMANDCHANGEDIRECTORY
#define COMMANDCHANGEDIRECTORY
#include "CommandParserInterface.h"
#include "CommandInterface.h"
#include "FileSystem.h"
#include <map>
#include <string>
class CommandChangeDirectory : public CommandInterface
{

    bool execute(vector<string> cmd, FileSystem& fileSys);
};

#endif