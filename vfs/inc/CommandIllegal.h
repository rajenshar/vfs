#ifndef COMMANDILLEGAL
#define COMMANDILLEGAL
#include "CommandInterface.h"
#include "CommandParserInterface.h"
#include "FileSystem.h"
#include <vector>
#include <memory>
#include <string>
using namespace std;

class CommandIllegal : public CommandInterface
{

    bool execute(vector<string> cmd, FileSystem& fileSys);
};

#endif