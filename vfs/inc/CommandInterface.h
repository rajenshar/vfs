#ifndef COMMANDINTERFACE
#define COMMANDINTERFACE
#include "FileSystemComponentInterface.h"
#include "FileSystem.h"
class CommandInterface
{
    public:
    virtual bool execute(vector<string> cmd, FileSystem& fileSys)=0;
};

#endif