#ifndef COMMANDLISTDIRECTORY
#define COMMANDLISTDIRECTORY
#include "CommandInterface.h"
#include "CommandParserInterface.h"
#include "CommandInterface.h"
#include "FileSystem.h"
#include <vector>
#include <memory>
#include <string>
using namespace std;

class CommandListDirectory : public CommandInterface
{

    bool execute(vector<string> cmd, FileSystem& fileSys);
};

#endif