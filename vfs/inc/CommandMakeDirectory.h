#ifndef COMMANDMAKEDIRECTORY
#define COMMANDMAKEDIRECTORY
#include "CommandInterface.h"
#include "CommandParserInterface.h"
#include "CommandInterface.h"
#include "FileSystem.h"

class CommandMakeDirectory : public CommandInterface
{

    bool execute(vector<string> cmd, FileSystem& fileSys);
};

#endif