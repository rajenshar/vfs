#ifndef COMMANDPARSER
#define COMMANDPARSER
#include "CommandInterface.h"
#include "FileSystemInterface.h"
#include "CommandParserInterface.h"
#include "FileSystem.h"
#include <iostream>
#include <memory>
#include <string>
#include <vector>
using namespace std;

class CommandParser : public CommandParserInterface
{
    FileSystem fileSystem_;   
    public:
    CommandParser();
    ~CommandParser();
    vector<string> readCommand(string cmd);
    shared_ptr<CommandInterface> getCommandType(string);
    void mountFileSystem(FileSystem& fs);
    bool executeCommand(vector<string> cmd);
              
};

#endif