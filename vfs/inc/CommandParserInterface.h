#ifndef COMMANDPARSERINTERFACE
#define COMMANDPARSERINTERFACE
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include "CommandInterface.h"
#include "FileSystem.h"

class CommandParserInterface
{
       public:
       virtual vector<string> readCommand(string cmd)=0;
       virtual shared_ptr<CommandInterface> getCommandType(string)=0;
       virtual void mountFileSystem(FileSystem& fs)=0; 
       virtual bool executeCommand(vector<string> cmd)=0;
      

};

#endif