
#ifndef FILESYSTEM
#define FILESYSTEM
#include "FileSystemComponentInterface.h"
#include<map>
class FileSystem 
{
	std::shared_ptr<FileSystemComponentInterface> rootDirectory_;
	std::shared_ptr<FileSystemComponentInterface> currentWorkingDirectory_;
    map<string,shared_ptr<FileSystemComponentInterface>> fileSystemComponents_;
    public :
	FileSystem();	
	~FileSystem(); 	    
	std::shared_ptr<FileSystemComponentInterface>  getRootDirectory();
	std::shared_ptr<FileSystemComponentInterface>  getCurrentWorkingDirectory();
	void  setCurrentWorkingDirectory(std::shared_ptr<FileSystemComponentInterface>);
	bool create(string name, char compType);
	bool changeDirectory(string name);
	bool list(string name);       	
};

#endif