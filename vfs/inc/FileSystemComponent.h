#ifndef FILESYSTEMCOMPONENT
#define FILESYSTEMCOMPONENT
#include "FileSystemComponentInterface.h"
#include "FileSystemComponent.h"
#include <string>
#include <vector>
#include <map>
class FileSystemComponent : public FileSystemComponentInterface
{
    string name_;
    int16_t size_;
    char componentType_;
    map<string,shared_ptr<FileSystemComponentInterface>> fileSystemComponents_;


	public :
	FileSystemComponent();
	~FileSystemComponent();
    string getName();
    void setName(string name);
    int16_t getSize();
    void setSize(int16_t size);
    void setComponentType(char componentType);
    char getComponentType();
    void create(std::shared_ptr<FileSystemComponentInterface> fileSysComp);
    std::shared_ptr<FileSystemComponentInterface> change(string name);
    void list();
    bool isFile();
    unique_ptr<FileSystemComponentInterface>& operator=(unique_ptr<FileSystemComponentInterface>&) = delete;


};



#endif
