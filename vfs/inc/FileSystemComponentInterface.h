#ifndef FILESYSTEMCOMPONENTINTERFACE
#define FILESYSTEMCOMPONENTINTERFACE
#include <memory>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

class FileSystemComponentInterface
{
	public:
    virtual void setSize(int16_t size)=0;
    virtual int16_t getSize()=0;
    virtual void setName(string name)=0;
    virtual string getName()=0;
    virtual void setComponentType(char)=0;
    virtual char getComponentType()=0;
    virtual void create(std::shared_ptr<FileSystemComponentInterface> fileSysComp)=0;
    virtual std::shared_ptr<FileSystemComponentInterface>  change(string name)=0;
    virtual void list()=0;
    bool virtual isFile()=0;
};

#endif