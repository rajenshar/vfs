#if 0
#ifndef FILESYSTEMINTERFACE
#define FILESYSTEMINTERFACE
#include "FileSystemComponentInterface.h"
#include <memory>
#include <iostream>
using namespace std;

class FileSystemInterface
{
	public:	
	virtual std::shared_ptr<FileSystemComponentInterface>  getRootDirectory()=0;
};

#endif
#endif