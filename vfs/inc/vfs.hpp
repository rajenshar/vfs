
#if 0
#ifndef _EXAMPLE_HPP
#define _EXAMPLE_HPP
#include <iostream>
#include <memory>
#include <string>
#include <vector>

using namespace std;


class IFileSystemComponent 
{
	public:
	virtual void executeCommand()=0;
    virtual void setSize(int16_t size)=0;
    virtual int16_t getSize()=0;
    virtual void setName(string name)=0;
    virtual string getName()=0;
};

class FileSystemComponent : public IFileSystemComponent
{
    string name_;
    int16_t size_;

	public :
	FileSystemComponent();
	~FileSystemComponent();
	void executeCommand();
    string getName();
    void setName(string name);
    int16_t getSize();
    void setSize(int16_t size);
    unique_ptr<IFileSystemComponent>& operator=(unique_ptr<IFileSystemComponent>&) = delete;


};





class FileSystem : public IFileSystem
{
	std::shared_ptr<IFileSystemComponent> rootDirectory_;
    
    public :
	FileSystem();	
	~FileSystem(); 	
    void executeCommand();
	std::shared_ptr<IFileSystemComponent>  getRootDirectory();
    //void  setRootDirectory(std::unique_ptr<IFileSystemComponent> fsc);
   
	
};

class ICommand
{
    public:
    virtual bool execute(vector<string> cmd, shared_ptr<IFileSystemComponent> fileSysComp)=0;
};

class ICommandParser
{
       public:
       virtual vector<string> readCommand(string cmd)=0;
       virtual shared_ptr<ICommand> getCommandType(string)=0;
       virtual void mountFileSystem(shared_ptr<IFileSystem> fs)=0; 
       virtual bool executeCommand(vector<string> cmd)=0;
      

};


class CommandParser : public ICommandParser
{
    shared_ptr<IFileSystemComponent> rootDir_;   
    public:
    CommandParser();
    ~CommandParser();
    vector<string> readCommand(string cmd);
    shared_ptr<ICommand> getCommandType(string);
    void mountFileSystem(shared_ptr<IFileSystem> fs);
    bool executeCommand(vector<string> cmd);
              
};

class CommandMakeDirectory : public ICommand
{

    bool execute(vector<string> cmd, std::shared_ptr<IFileSystemComponent> fileSysComp);
};

class CommandIllegal : public ICommand
{

    bool execute(vector<string> cmd, std::shared_ptr<IFileSystemComponent> fileSysComp);
};

#endif 
#endif