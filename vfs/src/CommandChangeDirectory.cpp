#include "CommandChangeDirectory.h"
#include "FileSystem.h"
bool CommandChangeDirectory :: execute(vector<string> cmd, FileSystem& fileSys)
{
    cout << "execute cd " << cmd[1]  << endl;
    //cout << cmd[1] << endl;
    bool result = false;
    result = fileSys.changeDirectory(cmd[1]);
    return result;
}
