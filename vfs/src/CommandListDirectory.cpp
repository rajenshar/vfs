
#include "CommandListDirectory.h"
#include "FileSystem.h"
bool CommandListDirectory :: execute(vector<string> cmd, FileSystem& fileSys)
{
    bool result = false;
    string dirname;
    cout << "execute ls" << endl;

    if (cmd.size() > 1){
        cout << cmd[1] << endl; 
        dirname = cmd[1];
    }
    else
    {
        dirname = ".";
    }
    
    result = fileSys.list(dirname);
    return result;
}
