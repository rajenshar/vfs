
#include "CommandMakeDirectory.h"
#include "FileSystemComponentInterface.h"

bool CommandMakeDirectory :: execute(vector<string> cmd, FileSystem& fileSys)
{
    cout << "execute mkdir " << cmd[1]  << endl;
    bool result = false;
       
    result = fileSys.create(cmd[1], 'D');
    return result;
}
