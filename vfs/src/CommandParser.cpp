#include "CommandParser.h"
#include "CommandInterface.h"
#include "CommandMakeDirectory.h"
#include "CommandListDirectory.h"
#include "CommandChangeDirectory.h"
#include "CommandIllegal.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

CommandParser::CommandParser()
{
    cout << "Command Parser Construction" << endl;
}

CommandParser::~CommandParser()
{
    cout << "Command Parser Destruction" << endl;
}

void CommandParser :: mountFileSystem(FileSystem& fs)
{
    //fileSystem_ = fs;
    
}

/**
 * ! important : Think through const correctness
 


*/

shared_ptr<CommandInterface> CommandParser::getCommandType(const string cmd)
 {

     if(cmd == "mkdir"){
      return shared_ptr<CommandInterface>(new CommandMakeDirectory());
     }
     else if(cmd == "ls"){
      return shared_ptr<CommandInterface>(new CommandListDirectory());       
     }
     else if(cmd == "cd"){
      return shared_ptr<CommandInterface>(new CommandChangeDirectory());       
     }
    else{
      return shared_ptr<CommandInterface>(new CommandIllegal());    
    }
    
 }


 bool CommandParser::executeCommand(vector<string> commandWithArgs)
 {
     bool result;
     shared_ptr<CommandInterface> cmd;
     cmd = getCommandType(commandWithArgs[0]);
     result = cmd->execute(commandWithArgs,fileSystem_);
     return result;

     
 }


vector<string> CommandParser :: readCommand(string command)
{

    vector<string> commandWithArgs;

    string item;
    stringstream ss(command);
    while(getline(ss, item, ' ')){

        commandWithArgs.push_back(move(item));
    }

    return commandWithArgs;
}
