#include "FileSystemComponentInterface.h"
#include "FileSystem.h"
#include "FileSystemComponent.h"

FileSystem :: FileSystem() 
{
   cout << "File System Constructor" << endl;
   rootDirectory_ = shared_ptr<FileSystemComponentInterface>(new FileSystemComponent());
   rootDirectory_->setName("/");
   rootDirectory_->setSize(0);
   rootDirectory_->setComponentType('D');
  //cout << "Assigning CWD with RootDir" << endl;
   currentWorkingDirectory_ = rootDirectory_;
   fileSystemComponents_["/"] = rootDirectory_;
   
}


FileSystem :: ~FileSystem()
{

   
}

std::shared_ptr<FileSystemComponentInterface>  FileSystem :: getRootDirectory()
{

    return rootDirectory_;
}

std::shared_ptr<FileSystemComponentInterface>  FileSystem :: getCurrentWorkingDirectory()
{

    cout << "GET CWD=" << currentWorkingDirectory_->getName() << endl;
    return currentWorkingDirectory_;
}


void FileSystem :: setCurrentWorkingDirectory(std::shared_ptr<FileSystemComponentInterface> cwd)
{

    currentWorkingDirectory_ = cwd;
    cout << "SET CWD=" << currentWorkingDirectory_->getName() << endl;
}

bool FileSystem :: create(string name, char compType)
{
       bool result = false;
       shared_ptr<FileSystemComponentInterface> dir  = shared_ptr<FileSystemComponentInterface>(new FileSystemComponent());
       if(dir != nullptr){
            dir->setName(name);
            dir->setSize(0);
            getCurrentWorkingDirectory()->create(dir);
            result = true;
       }
       
       return result;
}

bool FileSystem :: changeDirectory(string name)
{
       bool result;
       shared_ptr<FileSystemComponentInterface> dir = nullptr;

       if(name == "/")
       {

           setCurrentWorkingDirectory(getRootDirectory());
           return true;
       }

       dir = getCurrentWorkingDirectory()->change(name);

       if(dir != nullptr)
       {
           cout << "set working directory called" << endl;
           cout << "set working directory " << dir->getName() << endl;
            setCurrentWorkingDirectory(dir);
            getCurrentWorkingDirectory();

            if(dir->getName() == getCurrentWorkingDirectory()->getName())
            {

                result =  true;
            }
            else
            {
                result =  false;
            }
       }
       else
       {
           result = false;
       }

       return result;              
}

bool FileSystem :: list(string name)
{
    bool result;
    shared_ptr<FileSystemComponentInterface> fsc = nullptr;

    if(name == "/"){
        fsc = getRootDirectory();
    }
    else if (name == ".") {
	    fsc = getCurrentWorkingDirectory();
    }
    else
    {
        fsc = getCurrentWorkingDirectory()->change(name);
    }
    

    if (fsc != nullptr)
    {
        fsc->list();
        result = true;
    }
    else
    {
       cout << "DIRECTORY NOT FOUND" << endl;
       result = false;
    }
    
    return result;

}