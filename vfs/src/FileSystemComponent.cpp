#include "FileSystemComponent.h"
#include <iostream>
using namespace std;

FileSystemComponent :: FileSystemComponent()
{

    cout << "File System Component constructor" << endl;
}

FileSystemComponent :: ~FileSystemComponent()
{


}


int16_t FileSystemComponent :: getSize()
{
    return 0;    
}

void FileSystemComponent :: setSize(int16_t size)
{
      size_ = size;  
}

string FileSystemComponent :: getName()
{
    return name_;    
}

void FileSystemComponent :: setName(string name)
{
      name_ = name;  
}

void FileSystemComponent :: setComponentType(char componentType)
{
      componentType_ = componentType;  
}

char FileSystemComponent :: getComponentType()
{
    return componentType_;    
}

bool FileSystemComponent :: isFile()
{
    return (componentType_ == 'D' ? false : true); 
}

void FileSystemComponent :: create(std::shared_ptr<FileSystemComponentInterface> fileSysComp)
{
    cout << "create fsc" << fileSysComp->getName();

    fileSystemComponents_[fileSysComp->getName()] = fileSysComp;
}


std::shared_ptr<FileSystemComponentInterface> FileSystemComponent :: change( string name)
{
    map<string,std::shared_ptr<FileSystemComponentInterface>> ::iterator it;
    it = fileSystemComponents_.find(name);

    if (it != fileSystemComponents_.end()){  
        cout<<"changed directory to  = " << it->first <<endl;
        return it->second;
    }else{
        cout<<"Not found"<<endl;
        return nullptr;
    }
    
    
}


void FileSystemComponent :: list()
{
    map<string,std::shared_ptr<FileSystemComponentInterface>> ::iterator it = fileSystemComponents_.begin();
 
	while (it != fileSystemComponents_.end())
	{		
		std::cout << it->first << std::endl;

		it++;
	}
    
    
}

