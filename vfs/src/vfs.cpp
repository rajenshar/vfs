#include "vfs.hpp"
#if 0
FileSystemComponent :: FileSystemComponent()
{

    
}

FileSystemComponent :: ~FileSystemComponent()
{


}

void FileSystemComponent :: executeCommand()
{

    
}

int16_t FileSystemComponent :: getSize()
{
    return 0;    
}

void FileSystemComponent :: setSize(int16_t size)
{
      size_ = size;  
}

string FileSystemComponent :: getName()
{
    return name_;    
}

void FileSystemComponent :: setName(string name)
{
      name_ = name;  
}


FileSystem :: FileSystem() 
{

   rootDirectory_ = shared_ptr<IFileSystemComponent>(new FileSystemComponent());
   rootDirectory_->setName("/");
   rootDirectory_->setSize(0);
   
}


std::shared_ptr<IFileSystemComponent>  FileSystem :: getRootDirectory()
{

    return rootDirectory_;
}

void FileSystem :: executeCommand()
{

   
   
}

FileSystem :: ~FileSystem()
{

   
}


bool CommandMakeDirectory :: execute(vector<string> cmd, std::shared_ptr<IFileSystemComponent> fileSysComp)
{
    cout << "execute mkdir" << endl;
}

bool CommandIllegal :: execute(vector<string> cmd, std::shared_ptr<IFileSystemComponent> fileSysComp)
{
    cout << "Illegal comand" << endl;
}


CommandParser::CommandParser()
{
    cout << "Command Parser Construction" << endl;
}

CommandParser::~CommandParser()
{
    cout << "Command Parser Destruction" << endl;
}

void CommandParser :: mountFileSystem(shared_ptr<IFileSystem> fs)
{
    rootDir_ = fs->getRootDirectory();
    
}


shared_ptr<ICommand> CommandParser::getCommandType(string cmdWithArgs)
 {

     if(cmdWithArgs == "mkdir")
      return shared_ptr<ICommand>(new CommandMakeDirectory());    
   
    else    
        return shared_ptr<ICommand>(new CommandIllegal());    
    
 }


 bool CommandParser::executeCommand(vector<string> cmdWithArgs)
 {
     shared_ptr<ICommand> cmd;
     cmd = getCommandType(cmdWithArgs[0]);
     cmd->execute(cmdWithArgs,rootDir_);
       
 }


vector<string> CommandParser :: readCommand(string cmd)
{

    vector<string> test;
    test.push_back("mkdir");
    test.push_back("testdir");

    return test;
}

#endif
